# cbc-pe-demonstration

Produces a demonstration of how parameter estimation is used to understand
gravitational wave observations of compact binary coalescences.


## Installation

Download source code and install
```
$ git clone git@gitlab.com:dwysocki/cbc-pe-demonstration.git
$ cd cbc-pe-demonstration
$ pip install --user .
```

## Usage

Check the command line usage
```
usage: cbc_pe_demo [-h] --trial-masses TRIAL_MASSES [TRIAL_MASSES ...]
                   [--trial-masses-post-fit TRIAL_MASSES_POST_FIT [TRIAL_MASSES_POST_FIT ...]]
                   [--approximant APPROXIMANT] [--psd PSD] [--f-low F_LOW]
                   [--delta-f DELTA_F] [--duration DURATION]
                   [--color-obs COLOR_OBS] [--color-model COLOR_MODEL]
                   [--plot-format PLOT_FORMAT] [--mpl-backend MPL_BACKEND]
                   output_dir M_total

Create slides for demonstrating how parameter estimation (PE) for gravitational
wave signals from compact binarie coalescences (CBCs) is performed, comparing
models to data.

positional arguments:
  output_dir            Directory to output files to. Will create directory if
                        it doesn't exist.
  M_total               True total mass.

optional arguments:
  -h, --help            show this help message and exit
  --trial-masses TRIAL_MASSES [TRIAL_MASSES ...]
                        Sequence of trial M_total values to visualize.
  --trial-masses-post-fit TRIAL_MASSES_POST_FIT [TRIAL_MASSES_POST_FIT ...]
                        Sequence of trial M_total values to visualize after
                        fitting.
  --approximant APPROXIMANT
                        Waveform approximant model to use for signal and fit.
  --psd PSD             Name of PSD to use.
  --f-low F_LOW         Low frequency cutoff.
  --delta-f DELTA_F     Frequency bin size.
  --duration DURATION   Fix the plotted signal duration. Higher mass systems
                        should be set to a lower duration.
  --color-obs COLOR_OBS
                        Color to use for observations.
  --color-model COLOR_MODEL
                        Color to use for models.
  --plot-format PLOT_FORMAT
                        File format used for plotting.
  --mpl-backend MPL_BACKEND
                        Backend to use for matplotlib.

Example usage, fitting for a 50 solar mass binary black hole:
$ cbc_pe_demo output_dir 50.0 \
  --trial-masses 40.0 60.0 45.0 55.0 \
  --trial-masses-post-fit 48.0 52.0 49.0 51.0 50.0 \
  --duration 0.2
```


## Example

Running the example shown in the help message above, the following slides were
produced.

[![Demo slides hosted on Google Docs](example.png)](https://docs.google.com/presentation/d/e/2PACX-1vQU3LL-pei4uvJOIHfFB75rwLYzHowHcl7dKQe2OSIvIXvOShVxbf3rRvU24l4Zj-BbZreEG8696w69/pub?start=false&loop=false&delayms=3000)


## Limitations

Currently only demonstrates zero-noise case, with all parameters assumed to be
known except for total mass.  Future developments will first add in noise, and
eventually multiple dimensions may be used if it can be done clearly.  Could
also generalize to parameter estimation for arbitrary 1D problems.


## Credit

Copyright (c) 2020 Daniel Wysocki

This project is free to distribute under the MIT License.

This was made possible by [Python](https://www.python.org/),
[NumPy, SciPy, and Matplotlib](https://www.scipy.org/), and
[PyCBC](https://pycbc.org/).
