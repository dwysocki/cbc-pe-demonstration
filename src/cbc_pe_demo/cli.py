import argparse
import os
import numpy
import scipy.interpolate
import pycbc.filter
import pycbc.psd
import pycbc.waveform

from . import gen_noise

description = """\
Create slides for demonstrating how parameter estimation (PE) for gravitational
wave signals from compact binarie coalescences (CBCs) is performed, comparing
models to data.
"""

epilog = """\
Example usage, fitting for a 50 solar mass binary black hole:
$ cbc_pe_demo output_dir 50.0 \\
  --trial-masses 40.0 60.0 45.0 55.0 \\
  --trial-masses-post-fit 48.0 52.0 49.0 51.0 50.0 \\
  --duration 0.2
"""


def make_parser():
    parser = argparse.ArgumentParser(
        description=description, epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "output_dir",
        help="Directory to output files to.  Will create directory if it "
             "doesn't exist.",
    )

    parser.add_argument(
        "M_total", metavar="MASS_IN_MSUN",
        type=float,
        help="True total mass.",
    )

    parser.add_argument(
        "--trial-masses", metavar="MASS_IN_MSUN",
        type=float, nargs="+", required=True,
        help="Sequence of trial M_total values to visualize.",
    )
    parser.add_argument(
        "--trial-masses-post-fit", metavar="MASS_IN_MSUN",
        type=float, nargs="+",
        help="Sequence of trial M_total values to visualize after fitting.",
    )

    parser.add_argument(
        "--approximant",
        default="IMRPhenomPv2",
        help="Waveform approximant model to use for signal and fit.",
    )
    parser.add_argument(
        "--psd",
        default="aLIGOZeroDetHighPower",
        help="Name of PSD to use.",
    )
    parser.add_argument(
        "--f-low", metavar="FREQ_IN_HZ",
        type=float, default=15.0,
        help="Low frequency cutoff.",
    )
    parser.add_argument(
        "--delta-f", metavar="FREQ_IN_HZ",
        type=float, default=1.0/16.0,
        help="Frequency bin size.",
    )

    parser.add_argument(
        "--duration", metavar="TIME_IN_SEC",
        type=float,
        help="Fix the plotted signal duration.  Higher mass systems should be "
             "set to a lower duration.",
    )

    parser.add_argument(
        "--color-obs", metavar="COLOR",
        default="C0",
        help="Color to use for observations.",
    )
    parser.add_argument(
        "--color-model", metavar="COLOR",
        default="C3",
        help="Color to use for models.",
    )

    parser.add_argument(
        "--plot-format", metavar="FORMAT",
        default="png",
        help="File format used for plotting.",
    )

    parser.add_argument(
        "--mpl-backend", metavar="BACKEND",
        default="Agg",
        help="Backend to use for matplotlib.",
    )

    return parser


def main():
    cli_parser = make_parser()
    cli_args = cli_parser.parse_args()

    import matplotlib as mpl
    mpl.use(cli_args.mpl_backend)
    import matplotlib.pyplot as plt

    os.makedirs(cli_args.output_dir, exist_ok=True)

    # Setup figure.
    fig = plt.figure(figsize=(12,8), constrained_layout=True)
    gs = mpl.gridspec.GridSpec(
        2, 2,
        height_ratios=(2,1), width_ratios=(1,3),
        figure=fig,
    )
    ax_likelihood = fig.add_subplot(gs[0,:])
    ax_visualize = fig.add_subplot(gs[1,0])
    ax_timeseries = fig.add_subplot(gs[1,1])

    # Hide axes on black holes.
    ax_visualize.axis("off")

    # Label axes.
    ax_likelihood.set_xlabel(r"Total Mass [$M_\odot$]")
    ax_likelihood.set_ylabel("Match")
    ax_timeseries.set_xlabel(r"Time [s]")
    ax_timeseries.set_ylabel(r"Strain [$10^{-20}$]")

    # Construct PSD.
    flen = int(2048 / cli_args.delta_f) + 1
    psd_fn = getattr(pycbc.psd, cli_args.psd)
    psd = psd_fn(flen, cli_args.delta_f, cli_args.f_low)

#    noise_obs = gen_noise.get_noise(psd)
    # Generate and plot signal
    signal_obs, _ = pycbc.waveform.get_td_waveform(
        approximant=cli_args.approximant,
        mass1=0.5*cli_args.M_total, mass2=0.5*cli_args.M_total,
        f_lower=cli_args.f_low,
        delta_t=1.0/4096.0,
    )
    ax_timeseries.plot(
        signal_obs.sample_times, signal_obs*1e+20,
        color=cli_args.color_obs, linestyle="solid",
    )


    # Resize the waveforms to the same length
#    tlen = max(len(noise_obs), len(signal_obs))
#    noise_obs.resize(tlen)
#    signal_obs.resize(tlen)

    # Compute the observed data.
#    data_obs = signal_obs + noise_obs



    # Determine masses to plot interpolant on.
    M_min = numpy.min(cli_args.trial_masses)
    M_max = numpy.max(cli_args.trial_masses)
    if cli_args.trial_masses_post_fit is not None:
        M_min = min(M_min, numpy.min(cli_args.trial_masses_post_fit))
        M_max = max(M_max, numpy.max(cli_args.trial_masses_post_fit))
    M_total_smooth = numpy.linspace(M_min, M_max, 200)

    # Set scales for BBH visualization.  Radii are equal to their
    # masses, the separation distance is equal to the diameter of the smallest
    # system, and the plot is scaled so the largest system just barely fits.
    scale = 2.0*M_max + M_min
    ax_visualize.set_xlim([-0.5*scale, +0.5*scale])
    ax_visualize.set_ylim([-0.5*scale, +0.5*scale])

    # Initialize lists of plotted data and associated matplotlib artists to
    # hide/show for different subplots.
    M_totals = []
    lines = []
    matches = []
    points = []
    fit_lines = []
    binaries = []

    for M_total_trial in cli_args.trial_masses:
        # Compute and plot signal
        signal_model, _ = pycbc.waveform.get_td_waveform(
            approximant=cli_args.approximant,
            mass1=0.5*M_total_trial, mass2=0.5*M_total_trial,
            f_lower=cli_args.f_low,
            delta_t=1.0/4096.0,
        )

        line, = ax_timeseries.plot(
            signal_model.sample_times, signal_model*1e+20,
            color=cli_args.color_model, linestyle="dashed",
        )
        line.set_visible(False)
        lines.append(line)

        # Compute and plot match.
        tlen = max(len(signal_obs), len(signal_model))
        signal_obs.resize(tlen)
        signal_model.resize(tlen)

        match, _ = pycbc.filter.match(
            signal_obs, signal_model,
            psd=psd, low_frequency_cutoff=cli_args.f_low,
        )
        matches.append(match)
        point = ax_likelihood.scatter(
            [M_total_trial], [match],
            color="C0", marker="o", s=20,
        )
        point.set_visible(False)
        points.append(point)

        # Remember this M_total has been used.
        M_totals.append(M_total_trial)

        # No fit here.
        fit_lines.append(None)

        # Plot binary
        dist = 0.5 * (M_min + M_total_trial)
        obj1 = plt.Circle(
            (-dist, 0.0), 0.5*M_total_trial,
            facecolor="black", edgecolor="white",
            linestyle="dashed", linewidth=0.5,
        )
        obj1.set_visible(False)
        obj2 = plt.Circle(
            (+dist, 0.0), 0.5*M_total_trial,
            facecolor="black", edgecolor="white",
            linestyle="dashed", linewidth=0.5,
        )
        obj2.set_visible(False)
        ax_visualize.add_artist(obj1)
        ax_visualize.add_artist(obj2)
        binaries.append((obj1, obj2))


    # Create fit to starting points.
    ln_fit_fn = scipy.interpolate.interp1d(
        M_totals, numpy.log(matches),
        kind="cubic",
    )

    # Plot fit.
    fit_line, = ax_likelihood.plot(
        M_total_smooth, numpy.exp(ln_fit_fn(M_total_smooth)),
        linestyle="dashed", color="C3",
    )
    fit_line.set_visible(False)
    fit_lines.append(fit_line)

    # No other things to store here.
    lines.append(None)
    points.append(None)
    binaries.append(None)

    ## Overplot post-fit points.
    for M_total_trial in cli_args.trial_masses_post_fit:
        # Compute and plot signal
        signal_model, _ = pycbc.waveform.get_td_waveform(
            approximant=cli_args.approximant,
            mass1=0.5*M_total_trial, mass2=0.5*M_total_trial,
            f_lower=cli_args.f_low,
            delta_t=1.0/4096.0,
        )

        line, = ax_timeseries.plot(
            signal_model.sample_times, signal_model*1e+20,
            color=cli_args.color_model, linestyle="dashed",
        )
        line.set_visible(False)
        lines.append(line)

        # Compute and plot match.
        tlen = max(len(signal_obs), len(signal_model))
        signal_obs.resize(tlen)
        signal_model.resize(tlen)

        match, _ = pycbc.filter.match(
            signal_obs, signal_model,
            psd=psd, low_frequency_cutoff=cli_args.f_low,
        )
        matches.append(match)
        point = ax_likelihood.scatter(
            [M_total_trial], [match],
            color="C0", marker="o", s=20,
        )
        point.set_visible(False)
        points.append(point)

        # Remember this M_total has been used.
        M_totals.append(M_total_trial)

        # Create fit to points.
        ln_fit_fn = scipy.interpolate.interp1d(
            M_totals, numpy.log(matches),
            kind="cubic",
        )

        # Plot fit.
        fit_line, = ax_likelihood.plot(
            M_total_smooth, numpy.exp(ln_fit_fn(M_total_smooth)),
            linestyle="dashed", color="C3",
        )
        fit_line.set_visible(False)
        fit_lines.append(fit_line)

        # Plot binary
        dist = 0.5 * (M_min + M_total_trial)
        obj1 = plt.Circle(
            (-dist, 0.0), 0.5*M_total_trial,
            facecolor="black", edgecolor="white",
            linestyle="dashed", linewidth=0.5,
        )
        obj1.set_visible(False)
        obj2 = plt.Circle(
            (+dist, 0.0), 0.5*M_total_trial,
            facecolor="black", edgecolor="white",
            linestyle="dashed", linewidth=0.5,
        )
        obj2.set_visible(False)
        ax_visualize.add_artist(obj1)
        ax_visualize.add_artist(obj2)
        binaries.append((obj1, obj2))

    truncate_duration(ax_timeseries, cli_args.duration)

    fig.savefig(get_filename(cli_args.output_dir, 0, fmt=cli_args.plot_format))

    iterables = zip(lines, points, fit_lines, binaries)
    for i, (line, point, fit_line, binary) in enumerate(iterables):
        if line is not None:
            line.set_visible(True)
        if point is not None:
            point.set_visible(True)
        if fit_line is not None:
            fit_line.set_visible(True)
        if binary is not None:
            binary[0].set_visible(True)
            binary[1].set_visible(True)

        fig.savefig(
            get_filename(cli_args.output_dir, i+1, fmt=cli_args.plot_format)
        )

        if line is not None:
            line.set_visible(False)
        if fit_line is not None:
            fit_line.set_visible(False)
        if binary is not None:
            binary[0].set_visible(False)
            binary[1].set_visible(False)

def get_filename(directory, index, fmt="eps"):
    return os.path.join(directory, "cbc_pe_demo_{:03}.{}".format(index, fmt))


def truncate_duration(ax, duration):
    x_lo, x_hi = ax.get_xlim()

    if duration is None:
        ax.set_xlim([x_lo, 0.0])
    else:
        ax.set_xlim([-duration, 0.0])
