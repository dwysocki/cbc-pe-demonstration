import pycbc.noise
import pycbc.psd

def get_noise(psd, seed=0):
    import pycbc.noise

    # Generate 32 seconds of noise at 4096 Hz
    delta_t = 1.0 / 4096
    tsamples = int(32 / delta_t)
    noise = pycbc.noise.noise_from_psd(tsamples, delta_t, psd, seed=seed)

    return noise
